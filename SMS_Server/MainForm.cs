﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using MySQLDriverCS;
using SMSPDULib;
using System.Diagnostics;
using System.Xml;
using System.IO;


namespace SMS_Server
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private bool RunInDebug = false;
        public Ini.IniFile ini = new Ini.IniFile(Application.StartupPath + "\\SMS_Server.ini");
        public int RS232_AutoCheckCurrentDelay = 0;
        public MySQLConnection MySQL;
        public string UseCOM = "COM1";
        public string UseCOMspeed = "9600";
        public bool UseCOMautoConnect = false;
        int GateErrorCount = 0;
        int MySQLErrorCount = 0;
        string AppID = "";
        string OutQueuePath = Application.StartupPath + "\\outqueue";
        bool OutQueueProcessing = false;

        private void MainForm_Load(object sender, EventArgs e)
        {
            // APP INIT...
            string S;
            //int I = 16;
            S = ini.IniReadValue("global", "AppID");
            if (S != "") AppID = S;

            SMS_notifyIcon.Text = Application.ProductName.ToString() + " - " + Application.ProductVersion.ToString() + " | " + AppID;
            txt_Debug.Text = Application.ProductName + " - " + Application.ProductVersion.ToString() + " | " + AppID;
            this.Text = Application.ProductName.ToString() + " - " + Application.ProductVersion.ToString() + " | " + AppID;
            lbl_AppID.Text = this.Text;

            ini.IniWriteValue("global", "LastVersion", Application.ProductVersion.ToString());

            Log2EventLog("Starting - " + Application.ProductName + " - " + Application.ProductVersion.ToString(), EventLogEntryType.Information, 0);
            if (RunInDebug != false) LogDebug("DEBUG MODE ENABLED!!!", "NOTICE");

            DoAutoUpdate();

            S = ini.IniReadValue("COM", "Port");
            if (S != "") UseCOM = S;

            UseCOMspeed = "9600";
            S = ini.IniReadValue("COM", "PortSpeed");
            if (S != "") UseCOMspeed = S;

            S = ini.IniReadValue("COM", "AutoConnect");
            if (S == "True") UseCOMautoConnect = true;

            try
            {
                S = ini.IniReadValue("COM", "CheckInterval");
                if (S != "") RS232_AutoCheckInterval.Value = Convert.ToInt32(S);
            }
            catch { LogDebug("CheckInterval - invalid value", "INI"); }

            S = ini.IniReadValue("global", "WndState");
            if (S == "Minimized")
            {
                this.WindowState = FormWindowState.Minimized;
            }
            if (UseCOMautoConnect == true) RS232_Connect();

            // create OutQUEUE dir
            if (System.IO.Directory.Exists(OutQueuePath) == false)
            {
                try
                {
                    System.IO.Directory.CreateDirectory(OutQueuePath);
                }
                catch
                {
                    LogDebug("Can't create QUEUE directory!", "ERROR");
                }
            }
            // set QUEUE watcher...
            Logfile_watch.Path = OutQueuePath;
            Logfile_watch.Filter = "*.*";
            Logfile_watch.EnableRaisingEvents = true;
        }

        private void DoAutoUpdate()
        {
            // AUTO UPDATE (silent)
            bool AUrun = false;
            string cfg_auLastrun = ini.IniReadValue("AutoUpdate", "lastrun");
            if (cfg_auLastrun != "")
            {
                if (DateTime.Compare(DateTime.Today.AddDays(-2), Convert.ToDateTime(cfg_auLastrun)) > 0)
                {
                    AUrun = true;
                }
            }
            else { AUrun = true; }

            if (AUrun == true)
            {
                LogDebug("Looking for new version", "AU");

                SB.BgAutoUpdate.BgAutoUpdate au = new SB.BgAutoUpdate.BgAutoUpdate();
                au.au_url = "http://au.ladasoukup.cz/SMS_Server/";
                au.au_ico = Properties.Resources.ico_update;
                au.DoUpdate();

                ini.IniWriteValue("AutoUpdate", "lastrun", System.DateTime.Today.ToShortDateString());
            }
        }

        public void LogDebug(string dbgText, string dbgType)
        {
            int TrimLength = 2500;
            if (txt_Debug.Text.Length > TrimLength)
            {
                txt_Debug.Text = txt_Debug.Text.Substring((txt_Debug.Text.Length - TrimLength), TrimLength);
            }

            txt_Debug.AppendText("\r\n" + System.DateTime.Now.ToLongTimeString() + ": " + dbgType + ": " + dbgText);
        }

        public void Log2EventLog(string EvtMsg, EventLogEntryType EvtType, int EventID)
        {
            string EvtSource = Application.ProductName + " " + Application.ProductVersion.ToString();
            // LOG to EventLog
            try
            {
                EventLog aLog = new EventLog();
                aLog.Source = EvtSource;
                aLog.WriteEntry(EvtMsg, EvtType, EventID);
            }
            catch { }
        }

        public void ShowAppNotify(string msgText, int msgTime, ToolTipIcon msgIcon)
        {
            SMS_notifyIcon.BalloonTipTitle = "SMS server";
            SMS_notifyIcon.BalloonTipText = msgText;
            SMS_notifyIcon.BalloonTipIcon = msgIcon;
            SMS_notifyIcon.ShowBalloonTip(msgTime);
        }

        private void RS232_Connect()
        {
            string S = "";
            string Flush = "";
            
            SMS_progressTimer.Enabled = false;
            if (serialPort1.IsOpen == true)
            {
                serialPort1.Close();
                LogDebug("Already connected... Closing", "COM");
            }
            serialPort1.PortName = UseCOM;
            if (UseCOMspeed != "") serialPort1.BaudRate = Convert.ToInt32(UseCOMspeed);
            try
            {
                serialPort1.Open();
                Flush = serialPort1.ReadExisting();
                Application.DoEvents();
                serialPort1.Write("ATI\r\n");
                S = serialPort1.ReadTo("OK");
                LogDebug("Connected", "COM");

                Flush = serialPort1.ReadExisting();
                serialPort1.Write("AT+CMGF=0\r\n");
                S = serialPort1.ReadTo("OK");
                // LogDebug("Switching to PDU mode", "COM");
            }
            catch { 
                LogDebug("Can't connect...", "COM");
                Log2EventLog("Can't connect to COM port", EventLogEntryType.Warning, 11);
            }

            RS232_AutoCheckCurrentDelay = 0;
            SMS_progressTimer.Enabled = true;
        }

        public void DoReadWriteSMS() {
            //MySQL connect
            string sql_host = ini.IniReadValue("sql", "sql_host");
            string sql_user = ini.IniReadValue("sql", "sql_user");
            string sql_pass = ini.IniReadEncValue("sql", "sql_pass");
            string sql_db = ini.IniReadValue("sql", "sql_db");
            string sql_db_table = ini.IniReadValue("sql", "sql_db_table");
            string SMS_gate = ini.IniReadValue("SMS", "sms_gate");

            try { MySQL = new MySQLConnection(new MySQLConnectionString(sql_host, sql_db, sql_user, sql_pass).AsString); }
            catch {
                LogDebug("Connection to \"" + sql_host + "\" failed.", "SQL");
                Log2EventLog("Connection to \"" + sql_host + "\" failed.", EventLogEntryType.Warning, 12);
            }
            try
            {
                MySQL.Open();

                SMS_progressTimer.Enabled = false;

                RS232_CheckNewSMS(SMS_gate, sql_db_table);
                RS232_SendAllSMS(SMS_gate, sql_db_table);
                SMS_Parse_LongSMS(SMS_gate, sql_db_table);

                MySQL.Close();
                MySQL.Dispose();
                MySQLErrorCount = 0;
            }
            catch {
                LogDebug("Connection to MySQL failed!", "SQL");
                MySQLErrorCount++;
                if (MySQLErrorCount > 5) {
                    ShowAppNotify("Připojení k MySQL serveru opakovaně selhalo", 10, ToolTipIcon.Error);
                    Log2EventLog("Connection to \"" + sql_host + "\" failed.", EventLogEntryType.Warning, 2);
                }
            }
            DoAutoUpdate();
            SMS_progressTimer.Enabled = true;
            RS232_AutoCheckCurrentDelay = 0;
        }

        private string smsFromNumberFilter(string smsNumberFrom)
        {
            string S;

            S = ini.IniReadValue("FromNumberFilter", smsNumberFrom);
            if (S != "") smsNumberFrom = S;
            return (smsNumberFrom);
        }

        private void RS232_CheckNewSMS(string SMS_gate, string sql_db_table)
        {
            if (this.WindowState == FormWindowState.Minimized) this.Hide();

            string ReadedSMS = "";
            string[] NewSMS;
            string[] SQL_data;
            string Flush = "";
            string SMS_DELETE_ID = "";
            string S = "";
            string[] A;
            bool ReadyToDelete = false;
            string tmpSMS = "";
            string tmp_OrigSMS = "";
            string smsNumberFrom = "";

            if (serialPort1.IsOpen == true)
            {
                try
                {
                    Flush = serialPort1.ReadExisting();
                    serialPort1.Write("AT+CMGF=0\r\n");
                    Flush = serialPort1.ReadTo("OK");
                    // LogDebug("Switching to PDU mode", "COM");

                    // LogDebug("Reading new SMS...", "COM");
                    Flush = serialPort1.ReadExisting();
                    serialPort1.WriteLine("AT+CMGL=4\r\n");
                    ReadedSMS = serialPort1.ReadTo("OK");

                    // LogDebug("Parsing new SMS...", "SMS");

                    NewSMS = ReadedSMS.Split('\n');
                    SQL_data = new string[5];

                    bool StartFound = false;
                    for (int loop = 0; loop < (NewSMS.Length - 2); loop++)
                    {
                        if (NewSMS[loop].Length > 5)
                        {
                            if (NewSMS[loop].Substring(0, 5) == "+CMGL") StartFound = true;
                        }

                        if (StartFound)
                        {
                            SMSType smsType = SMSBase.GetSMSType(NewSMS[loop + 1]);
                            if (smsType == SMSType.SMS)
                            {
                                SMS sms = new SMS();
                                try
                                {
                                    // DEBUG!!!
                                    tmpSMS = NewSMS[loop + 1].Trim();
                                    tmp_OrigSMS = tmpSMS;
                                    //Log2EventLog("New SMS...\r\nPDU: " + tmpSMS.ToString(), EventLogEntryType.Information, 1);
                                    SMS.Fetch(sms, ref tmpSMS);
                                }
                                catch (Exception ex) {
                                    LogDebug("Wrong SMS data!!!", "ERROR");
                                    LogDebug(NewSMS[loop + 1], "PDU");
                                    Log2EventLog("Wrong SMS data!!!\r\nPDU: " + tmp_OrigSMS.ToString(), EventLogEntryType.Error, 10);

                                }
                                A = NewSMS[loop].Split(':');
                                A = A[1].Split(',');
                                S = A[0].Trim();
                                SQL_data[0] = S;
                                SQL_data[1] = smsFromNumberFilter(sms.PhoneNumber);
                                SQL_data[2] = sms.ServiceCenterTimeStamp.Year.ToString("0000") + sms.ServiceCenterTimeStamp.Month.ToString("00") + sms.ServiceCenterTimeStamp.Day.ToString("00");
                                SQL_data[3] = sms.ServiceCenterTimeStamp.Hour.ToString("00") + ":" + sms.ServiceCenterTimeStamp.Minute.ToString("00");
                                SQL_data[4] = sms.Message;
                                loop++;

                                ReadyToDelete = false;
                                if (sms.InParts == true)
                                {
                                    // Multipart SMS...
                                    if (SMS_Multipart_StoreToDB(SQL_data, sms.InPartsID, sms.Part, SMS_gate, sql_db_table) == true)
                                        ReadyToDelete = true;
                                }
                                else
                                {
                                    // Single SMS...
                                    if (SMS_StoreToDB(SQL_data, SMS_gate, sql_db_table) == true)
                                        ReadyToDelete = true;
                                }

                                if (ReadyToDelete)
                                {
                                    if (RunInDebug == false)
                                    {
                                        Flush = serialPort1.ReadExisting();
                                        SMS_DELETE_ID = SQL_data[0].ToString();
                                        if (SMS_DELETE_ID == "")
                                        {
                                            SMS_DELETE_ID = (Convert.ToInt32(ini.IniReadValue("SMS", "LastDeletedID")) + 1).ToString();
                                        }
                                        try
                                        {
                                            serialPort1.WriteLine("AT+CMGD=" + SMS_DELETE_ID + "\r\n");
                                            serialPort1.ReadTo("OK");
                                        }
                                        catch
                                        {
                                            try
                                            {
                                                serialPort1.WriteLine("AT+CMGD=0\r\n");
                                                serialPort1.ReadTo("OK");
                                            }
                                            catch { }
                                        }
                                        ini.IniWriteValue("SMS", "LastDeletedID", SMS_DELETE_ID);
                                        SMS_DELETE_ID = "";
                                    }
                                    else
                                    {
                                        LogDebug("SMS WAS NOT DELETED FROM SIM", "DEBUG");
                                    }
                                    LogDebug("New SMS from: " + SQL_data[1], "SMS");
                                    StartFound = false;
                                }
                            }
                        }
                    }
                    GateErrorCount = 0;
                }
                catch (Exception ex) {
                    LogDebug("Error reading data from RS232. " + ex.ToString(), "COM");
                    GateErrorCount++;
                    if (GateErrorCount > 3) {
                        ShowAppNotify("Připojení k SMS bráně opakovaně selhalo!", 10, ToolTipIcon.Error);
                        Log2EventLog("Připojení k SMS bráně opakovaně selhalo!", EventLogEntryType.Warning, 1);
                    }
                }
            }
            else
            {
                LogDebug("NOT CONNECTED!", "COM");
                RS232_Connect();
            }
        }

        public bool SMS_StoreToDB(string[] SMS, string SMS_gate, string sql_db_table)
        {
            bool Ret = false;
            try
            {
                object[,] SQL_data = new object[,] {
                    { "sms_gate", SMS_gate },
                    { "sms_from", SMS[1] },
                    { "sms_date", SMS[2] },
                    { "sms_time", SMS[3] },
                    { "sms_folder", "0" },
                    { "sms_text", SMS[4]}
                };
                try
                {
                    new MySQLInsertCommand(MySQL, SQL_data, sql_db_table + "_sms");
                }
                catch (Exception ex) {
                    LogDebug(ex.ToString(), "SQL-ERR");
                    Log2EventLog(ex.ToString(), EventLogEntryType.Error, 101);
                }


                Ret = true;
            }
            catch (Exception ex) {
                LogDebug(ex.ToString(), "ERROR");
                Log2EventLog(ex.ToString(), EventLogEntryType.Error, 102);
            }
            return Ret;
        }

        public bool SMS_Multipart_StoreToDB(string[] SMS, int SMS_InPartsID, int SMS_Part, string SMS_gate, string sql_db_table)
        {
            bool Ret = false;

            DateTime hold_until_raw = DateTime.Now.AddMinutes(2);
            string hold_until = hold_until_raw.Year.ToString("0000") + hold_until_raw.Month.ToString("00") + hold_until_raw.Day.ToString("00") + hold_until_raw.Hour.ToString("00") + hold_until_raw.Minute.ToString("00") + hold_until_raw.Second.ToString("00");

            try
            {
                object[,] SQL_data = new object[,] {
                    { "sms_gate", SMS_gate },
                    { "sms_from", SMS[1] },
                    { "sms_date", SMS[2] },
                    { "sms_time", SMS[3] },
                    { "sms_folder", "0" },
                    { "sms_text", SMS[4] },
                    { "sms_part_id", SMS_InPartsID },
                    { "sms_part", SMS_Part },
                    { "sms_hold_until", hold_until }
                };
                try
                {
                    new MySQLInsertCommand(MySQL, SQL_data, sql_db_table + "_sms_tmp");
                }
                catch (Exception ex) {
                    LogDebug(ex.ToString(), "SQL-ERR");
                    Log2EventLog(ex.ToString(), EventLogEntryType.Error, 101);
                }
                Ret = true;
            }
            catch (Exception ex) {
                LogDebug(ex.ToString(), "ERROR");
                Log2EventLog(ex.ToString(), EventLogEntryType.Error, 102);
            }
            return Ret;
        }

        private void SMS_Parse_LongSMS(string SMS_gate, string sql_db_table)
        {
            string now_raw = DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00") + DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00") + DateTime.Now.Second.ToString("00");
            string[] SQL_data = new string[5];
            string SMS_text = "";
            string SMS_from = "+420000000000";

            try
            {

                string sql_query = "SELECT DISTINCT sms_part_id, sms_from FROM " + sql_db_table + "_sms_tmp WHERE sms_hold_until < '" + now_raw + "' AND sms_gate='" + SMS_gate + "'";
                MySQLCommand sql_cmd = new MySQLCommand(sql_query, MySQL);
                MySQLDataReader sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    SMS_text = "";

                    int tmp_sms_part_id = sql_reader.GetInt32(0);
                    string tmp_sms_from = sql_reader.GetString(1);

                    string sql_query2 = "SELECT * FROM " + sql_db_table + "_sms_tmp WHERE sms_part_id=" + tmp_sms_part_id + " AND sms_gate='" + SMS_gate + "' AND sms_from='" + tmp_sms_from + "' ORDER BY sms_part DESC";
                    MySQLCommand sql_cmd2 = new MySQLCommand(sql_query2, MySQL);
                    MySQLDataReader sql_reader2 = sql_cmd2.ExecuteReaderEx();
                    while (sql_reader2.Read())
                    {
                        SMS_text = sql_reader2.GetString(6) + SMS_text;
                        SMS_from = sql_reader2.GetString(2);
                    }
                    SQL_data[0] = "";
                    SQL_data[1] = SMS_from;
                    SQL_data[2] = DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00");
                    SQL_data[3] = DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00");
                    SQL_data[4] = SMS_text;
                    if (SMS_StoreToDB(SQL_data, SMS_gate, sql_db_table) == true)
                    {
                        // DELETE FROM TMP table
                        string sql_query_del = "DELETE FROM " + sql_db_table + "_sms_tmp WHERE sms_part_id=" + tmp_sms_part_id + " AND sms_gate='" + SMS_gate + "' AND sms_from='" + tmp_sms_from + "'";
                        MySQLCommand sql_cmd_del = new MySQLCommand(sql_query_del, MySQL);
                        sql_cmd_del.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex) {
                LogDebug("Error parsing MultiPart SMS", "SMS");
                Log2EventLog(ex.ToString(), EventLogEntryType.Error, 103);
            }
        }

        private void RS232_SendAllSMS(string SMS_gate, string sql_db_table)
        {
            string smsSendIDfolder = ini.IniReadValue("SMS", "sms_sendID");

            try
            {
                serialPort1.ReadExisting();
                serialPort1.Write("AT+CMGF=1\r\n");
                serialPort1.ReadTo("OK");
                // LogDebug("Switching to TEXT mode", "COM");
                serialPort1.ReadExisting();

                string sql_query = "SELECT * FROM " + sql_db_table + "_sms_outqueue WHERE sms_gate='" + SMS_gate + "' AND sms_send_after < '" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Second + "' LIMIT 25";
                MySQLCommand sql_cmd = new MySQLCommand(sql_query, MySQL);
                MySQLDataReader sql_reader = sql_cmd.ExecuteReaderEx();
                while (sql_reader.Read())
                {
                    LogDebug("sending to: " + sql_reader["sms_to"], "SMS");
                    try
                    {
                        serialPort1.ReadExisting();
                        serialPort1.Write("AT+CMGS=\"" + sql_reader["sms_to"] + "\"\r\n");
                        serialPort1.ReadTo(">");
                        serialPort1.ReadExisting();
                        serialPort1.Write(sql_reader.GetString(3));
                        serialPort1.Write(Convert.ToChar(26) + "\r\n");
                        serialPort1.ReadTo("OK");

                        sql_query = "DELETE FROM " + sql_db_table + "_sms_outqueue WHERE id='" + sql_reader[0] + "'";
                        sql_cmd = new MySQLCommand(sql_query, MySQL);
                        sql_cmd.ExecuteNonQuery();
                        if ((smsSendIDfolder != "") && (smsSendIDfolder != "0"))
                        {
                            object[,] sql_InsertData = {
                            {"sms_gate", SMS_gate},
                            {"sms_from", sql_reader["sms_to"]},
                            {"sms_date", DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")},
                            {"sms_time", DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00")},
                            {"sms_folder", smsSendIDfolder},
                            {"sms_text", "ODESLANÁ SMS -> " + sql_reader.GetString(3)}
                        };
                            new MySQLInsertCommand(MySQL, sql_InsertData, sql_db_table + "_sms");
                        }
                        Application.DoEvents();
                    }
                    catch
                    {
                        LogDebug("SMS send failed :(", "ERROR");
                        Log2EventLog("SMS send failed\r\n" + sql_reader["sms_to"] + "\r\n" + sql_reader.GetString(3), EventLogEntryType.Error, 104);

                        if (sql_reader.GetInt16(5) > 5)
                        {
                            sql_query = "DELETE FROM " + sql_db_table + "_sms_outqueue WHERE id='" + sql_reader[0] + "'";
                            sql_cmd = new MySQLCommand(sql_query, MySQL);
                            sql_cmd.ExecuteNonQuery();
                            object[,] sql_InsertData = {
                                    {"sms_gate", SMS_gate},
                                    {"sms_from", sql_reader["sms_to"]},
                                    {"sms_date", DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00")},
                                    {"sms_time", DateTime.Now.Hour.ToString("00") + ":" + DateTime.Now.Minute.ToString("00")},
                                    {"sms_folder", "0"},
                                    {"sms_text", "ODESLÁNÍ SELHALO -> " + sql_reader.GetString(3)}
                            };
                            new MySQLInsertCommand(MySQL, sql_InsertData, sql_db_table + "_sms");
                        }
                        else
                        {
                            sql_query = "UPDATE " + sql_db_table + "_sms_outqueue set sms_error_count='" + (sql_reader.GetInt16(5) + 1) + "' WHERE id='" + sql_reader[0] + "'";
                            sql_cmd = new MySQLCommand(sql_query, MySQL);
                            sql_cmd.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch {
                LogDebug("Unknown error while sending messages...", "ERROR");
                Log2EventLog("Unknown error while sending messages...", EventLogEntryType.Error, 104);
            }
        }

        private void SMS_ProcessOutQueue()
        {
            string BasePath = Application.StartupPath + "\\outqueue\\";
            string[] xml_files = null;

            try
            {
                xml_files = Directory.GetFiles(BasePath);
                foreach (string file in xml_files)
                {
                    SMS_ProcessOutQueue(file);
                }
            }
            catch (Exception ex) { LogDebug(ex.ToString(), "ERROR"); }
        }

        private void SMS_ProcessOutQueue(string FullPath)
        {
            string QueueSMS_to;
            bool ValidXML = true;
            bool CanDeleteFile = false;
            bool B = false;

            OutQueueProcessing = true;
            System.Threading.Thread.Sleep(250);
            try
            {
                XmlTextReader xml_SMS_reader = new XmlTextReader(FullPath);
                XmlDocument xml_SMS = new XmlDocument();
                try
                {
                    xml_SMS.Load(xml_SMS_reader);
                }
                catch { ValidXML = false; }
                if (ValidXML)
                {
                    XmlNode xml_SMS_element = xml_SMS.DocumentElement;

                    XmlNodeList xml_SMS_to = xml_SMS_element.SelectNodes("/sms/to/text()");
                    XmlNodeList xml_SMS_text = xml_SMS_element.SelectNodes("/sms/text/text()");

                    CanDeleteFile = false;
                    for (int loop = 0; loop < xml_SMS_to.Count; loop++)
                    {
                        QueueSMS_to = xml_SMS_to[loop].Value;
                        QueueSMS_to = QueueSMS_to.Replace(" ", "");
                        QueueSMS_to = QueueSMS_to.Trim();
                        if (QueueSMS_to.Length == 9) QueueSMS_to = "+420" + QueueSMS_to;
                        if (QueueSMS_to.Length == 13)
                        {
                            LogDebug(QueueSMS_to + " - " + xml_SMS_text[0].Value, "XML Out Queue");
                            B = SMS_StoreInDB(QueueSMS_to, xml_SMS_text[0].Value);
                            if ((CanDeleteFile != true) && (B == true)) CanDeleteFile = true;
                        }
                        else
                        {
                            LogDebug("Wrong SMS number: " + QueueSMS_to, "XML Out Queue");
                        }
                    }
                }
                else
                {
                    LogDebug("Invalid XML data", "XML Out Queue");
                    CanDeleteFile = true;
                }
                if (CanDeleteFile)
                {
                    xml_SMS_reader.Close();
                    System.IO.File.Delete(FullPath);
                }
            }
            catch (Exception ex)
            {
                LogDebug(ex.ToString(), "XML Out Queue");
            }
            
            Application.DoEvents();
            OutQueueProcessing = false;
        }

        private bool SMS_StoreInDB(string Queue_SMS_number, string Queue_SMS_text)
        {
            bool ret = false;

            //MySQL connect
            string sql_host = ini.IniReadValue("sql", "sql_host");
            string sql_user = ini.IniReadValue("sql", "sql_user");
            string sql_pass = ini.IniReadEncValue("sql", "sql_pass");
            string sql_db = ini.IniReadValue("sql", "sql_db");
            string sql_db_table = ini.IniReadValue("sql", "sql_db_table");
            string SMS_gate = ini.IniReadValue("SMS", "sms_gate");

            try { MySQL = new MySQLConnection(new MySQLConnectionString(sql_host, sql_db, sql_user, sql_pass).AsString); }
            catch
            {
                LogDebug("Connection to \"" + sql_host + "\" failed.", "SQL");
                Log2EventLog("Connection to \"" + sql_host + "\" failed.", EventLogEntryType.Warning, 12);
            }
            try
            {
                MySQL.Open();

                object[,] sql_InsertData = {
                                    {"sms_gate", SMS_gate},
                                    {"sms_to", Queue_SMS_number},
                                    {"sms_text", Queue_SMS_text}
                            };
                new MySQLInsertCommand(MySQL, sql_InsertData, sql_db_table + "_sms_outqueue");
                ret = true;

                MySQL.Close();
                MySQL.Dispose();
                MySQLErrorCount = 0;
            }
            catch
            {
                ret = false;
                LogDebug("Connection to MySQL failed!", "SQL");
            }

            return ret;
        }

        private void btn_connect_Click(object sender, EventArgs e)
        {
            RS232_Connect();
        }

        private void btn_CheckSMSNow_Click(object sender, EventArgs e)
        {
            LogDebug("Manual check for new SMS...", "SMS");
            SMS_ProcessOutQueue();
            DoReadWriteSMS();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            int AutoCheckIntervalMax = Convert.ToInt32(RS232_AutoCheckInterval.Value);
            
            RS232_AutoCheckCurrentDelay += SMS_progressTimer.Interval / 1000;
            if (RS232_AutoCheckCurrentDelay >= AutoCheckIntervalMax)
            {
                SMS_ProcessOutQueue();
                DoReadWriteSMS();
            }
            try
            {
                RS232_AutoCheck_progress.Maximum = AutoCheckIntervalMax;
                RS232_AutoCheck_progress.Value = RS232_AutoCheckCurrentDelay;
            }
            catch { LogDebug("ProgressBar: \"RS232_AutoCheck_progress\"", "ERROR"); }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ini.IniWriteValue("COM", "CheckInterval", RS232_AutoCheckInterval.Value.ToString());
            ini.IniWriteValue("global", "WndState", this.WindowState.ToString());
            Log2EventLog("Application exit", EventLogEntryType.Warning, 0);
            //TODO: Write Log.
        }

        private void btn_cfg_Click(object sender, EventArgs e)
        {
            cfg WndCfg = new cfg(this);
            WndCfg.Show();
        }

        private void SMS_notifyIcon_Click(object sender, EventArgs e)
        {
            //notifyIcon_menu_showapp_Click(sender, e);
        }
        private void notifyIcon_menu_showapp_Click(object sender, EventArgs e)
        {
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void MainForm_Resize(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Minimized) this.Hide();
        }

        private void notifyIcon_menu_close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
        private void Logfile_watch_Created(object sender, System.IO.FileSystemEventArgs e)
        {
            LogDebug("New file detected in OutQueue: \"" + e.Name.ToString() + "\"", "OutQueue");
            SMS_ProcessOutQueue(e.FullPath);
        }
    }
}