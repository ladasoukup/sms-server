﻿namespace SMS_Server
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.txt_Debug = new System.Windows.Forms.TextBox();
            this.btn_connect = new System.Windows.Forms.Button();
            this.SMS_progressTimer = new System.Windows.Forms.Timer(this.components);
            this.btn_CheckSMSNow = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.RS232_AutoCheckInterval = new System.Windows.Forms.NumericUpDown();
            this.RS232_AutoCheck_progress = new System.Windows.Forms.ProgressBar();
            this.btn_cfg = new System.Windows.Forms.Button();
            this.SMS_notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.SMS_notifyIcon_menu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.notifiIcon_menu_CheckSMS = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon_menu_showapp = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon_menu_close = new System.Windows.Forms.ToolStripMenuItem();
            this.lbl_AppID = new System.Windows.Forms.Label();
            this.Logfile_watch = new System.IO.FileSystemWatcher();
            ((System.ComponentModel.ISupportInitialize)(this.RS232_AutoCheckInterval)).BeginInit();
            this.SMS_notifyIcon_menu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logfile_watch)).BeginInit();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 19200;
            this.serialPort1.DtrEnable = true;
            this.serialPort1.ReadTimeout = 15000;
            this.serialPort1.RtsEnable = true;
            // 
            // txt_Debug
            // 
            this.txt_Debug.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txt_Debug.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txt_Debug.Location = new System.Drawing.Point(2, 63);
            this.txt_Debug.MaxLength = 25000;
            this.txt_Debug.Multiline = true;
            this.txt_Debug.Name = "txt_Debug";
            this.txt_Debug.ReadOnly = true;
            this.txt_Debug.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txt_Debug.Size = new System.Drawing.Size(476, 193);
            this.txt_Debug.TabIndex = 0;
            // 
            // btn_connect
            // 
            this.btn_connect.Location = new System.Drawing.Point(368, 7);
            this.btn_connect.Name = "btn_connect";
            this.btn_connect.Size = new System.Drawing.Size(110, 21);
            this.btn_connect.TabIndex = 1;
            this.btn_connect.Text = "Připojit";
            this.btn_connect.UseVisualStyleBackColor = true;
            this.btn_connect.Click += new System.EventHandler(this.btn_connect_Click);
            // 
            // SMS_progressTimer
            // 
            this.SMS_progressTimer.Interval = 5000;
            this.SMS_progressTimer.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // btn_CheckSMSNow
            // 
            this.btn_CheckSMSNow.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_CheckSMSNow.Location = new System.Drawing.Point(199, 7);
            this.btn_CheckSMSNow.Name = "btn_CheckSMSNow";
            this.btn_CheckSMSNow.Size = new System.Drawing.Size(163, 21);
            this.btn_CheckSMSNow.TabIndex = 2;
            this.btn_CheckSMSNow.Text = "Zkontrolovat okamžitě";
            this.btn_CheckSMSNow.UseVisualStyleBackColor = true;
            this.btn_CheckSMSNow.Click += new System.EventHandler(this.btn_CheckSMSNow_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Kontrolovat SMS každých:";
            // 
            // RS232_AutoCheckInterval
            // 
            this.RS232_AutoCheckInterval.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.RS232_AutoCheckInterval.Location = new System.Drawing.Point(141, 7);
            this.RS232_AutoCheckInterval.Maximum = new decimal(new int[] {
            600,
            0,
            0,
            0});
            this.RS232_AutoCheckInterval.Minimum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.RS232_AutoCheckInterval.Name = "RS232_AutoCheckInterval";
            this.RS232_AutoCheckInterval.Size = new System.Drawing.Size(52, 21);
            this.RS232_AutoCheckInterval.TabIndex = 7;
            this.RS232_AutoCheckInterval.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // RS232_AutoCheck_progress
            // 
            this.RS232_AutoCheck_progress.Location = new System.Drawing.Point(6, 36);
            this.RS232_AutoCheck_progress.Name = "RS232_AutoCheck_progress";
            this.RS232_AutoCheck_progress.Size = new System.Drawing.Size(356, 21);
            this.RS232_AutoCheck_progress.Step = 1;
            this.RS232_AutoCheck_progress.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.RS232_AutoCheck_progress.TabIndex = 8;
            // 
            // btn_cfg
            // 
            this.btn_cfg.Location = new System.Drawing.Point(368, 36);
            this.btn_cfg.Name = "btn_cfg";
            this.btn_cfg.Size = new System.Drawing.Size(110, 21);
            this.btn_cfg.TabIndex = 9;
            this.btn_cfg.Text = "Nastavení";
            this.btn_cfg.UseVisualStyleBackColor = true;
            this.btn_cfg.Click += new System.EventHandler(this.btn_cfg_Click);
            // 
            // SMS_notifyIcon
            // 
            this.SMS_notifyIcon.ContextMenuStrip = this.SMS_notifyIcon_menu;
            this.SMS_notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("SMS_notifyIcon.Icon")));
            this.SMS_notifyIcon.Text = "SMS server";
            this.SMS_notifyIcon.Visible = true;
            this.SMS_notifyIcon.Click += new System.EventHandler(this.SMS_notifyIcon_Click);
            // 
            // SMS_notifyIcon_menu
            // 
            this.SMS_notifyIcon_menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.notifiIcon_menu_CheckSMS,
            this.notifyIcon_menu_showapp,
            this.notifyIcon_menu_close});
            this.SMS_notifyIcon_menu.Name = "SMS_notifyIcon_menu";
            this.SMS_notifyIcon_menu.Size = new System.Drawing.Size(177, 70);
            // 
            // notifiIcon_menu_CheckSMS
            // 
            this.notifiIcon_menu_CheckSMS.Name = "notifiIcon_menu_CheckSMS";
            this.notifiIcon_menu_CheckSMS.Size = new System.Drawing.Size(176, 22);
            this.notifiIcon_menu_CheckSMS.Text = "Zkontrolovat SMS";
            this.notifiIcon_menu_CheckSMS.Click += new System.EventHandler(this.btn_CheckSMSNow_Click);
            // 
            // notifyIcon_menu_showapp
            // 
            this.notifyIcon_menu_showapp.Name = "notifyIcon_menu_showapp";
            this.notifyIcon_menu_showapp.Size = new System.Drawing.Size(176, 22);
            this.notifyIcon_menu_showapp.Text = "Ukázat";
            this.notifyIcon_menu_showapp.Click += new System.EventHandler(this.notifyIcon_menu_showapp_Click);
            // 
            // notifyIcon_menu_close
            // 
            this.notifyIcon_menu_close.Image = global::SMS_Server.Properties.Resources.quit;
            this.notifyIcon_menu_close.Name = "notifyIcon_menu_close";
            this.notifyIcon_menu_close.Size = new System.Drawing.Size(176, 22);
            this.notifyIcon_menu_close.Text = "Zavřít";
            this.notifyIcon_menu_close.Click += new System.EventHandler(this.notifyIcon_menu_close_Click);
            // 
            // lbl_AppID
            // 
            this.lbl_AppID.Location = new System.Drawing.Point(3, 259);
            this.lbl_AppID.Name = "lbl_AppID";
            this.lbl_AppID.Size = new System.Drawing.Size(475, 17);
            this.lbl_AppID.TabIndex = 10;
            this.lbl_AppID.Text = "---";
            // 
            // Logfile_watch
            // 
            this.Logfile_watch.EnableRaisingEvents = true;
            this.Logfile_watch.NotifyFilter = System.IO.NotifyFilters.FileName;
            this.Logfile_watch.SynchronizingObject = this;
            this.Logfile_watch.Created += new System.IO.FileSystemEventHandler(this.Logfile_watch_Created);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 277);
            this.Controls.Add(this.lbl_AppID);
            this.Controls.Add(this.btn_cfg);
            this.Controls.Add(this.RS232_AutoCheck_progress);
            this.Controls.Add(this.RS232_AutoCheckInterval);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_CheckSMSNow);
            this.Controls.Add(this.btn_connect);
            this.Controls.Add(this.txt_Debug);
            this.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "SMS server";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Resize += new System.EventHandler(this.MainForm_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.RS232_AutoCheckInterval)).EndInit();
            this.SMS_notifyIcon_menu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Logfile_watch)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TextBox txt_Debug;
        private System.Windows.Forms.Button btn_connect;
        private System.Windows.Forms.Timer SMS_progressTimer;
        private System.Windows.Forms.Button btn_CheckSMSNow;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown RS232_AutoCheckInterval;
        private System.Windows.Forms.ProgressBar RS232_AutoCheck_progress;
        private System.Windows.Forms.Button btn_cfg;
        private System.Windows.Forms.NotifyIcon SMS_notifyIcon;
        private System.Windows.Forms.ContextMenuStrip SMS_notifyIcon_menu;
        private System.Windows.Forms.ToolStripMenuItem notifyIcon_menu_close;
        private System.Windows.Forms.ToolStripMenuItem notifyIcon_menu_showapp;
        private System.Windows.Forms.ToolStripMenuItem notifiIcon_menu_CheckSMS;
        private System.Windows.Forms.Label lbl_AppID;
        private System.IO.FileSystemWatcher Logfile_watch;
    }
}

