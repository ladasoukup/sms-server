using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Net;
using CodeProject.ReiMiyasaka;

namespace SB.BgAutoUpdate
{
    public class BgAutoUpdate
    {
        // THIS IS BACKGROUND AUTO UPDATE CLASS
        // (c)2006, Ladislav Soukup  [root@soundboss.cz]

        //GLOBALS...
        public string au_ico_Text = Application.ProductName + " - aktualizace";
        public string au_url = "http://www.example.com/"; // URL of AU site
        public System.Drawing.Icon au_ico = null;
        public bool ForceAU = false;

        bool au_error = false;
        string au_log = null;
        string[] auFilesToUpdate = new string[50];
        string[] auFilesToUpdate_check = new string[50];
        int auFilesToUpdateCount = 0;
        NotifyIcon ico = new NotifyIcon();

        //GLOBALS end


        private void SaveAuLog()
        {
            using (StreamWriter sw = new StreamWriter(Application.StartupPath + "\\log_au.log",false))
            {
                sw.Write(au_log);
            }
            Log2EventLog(au_log, EventLogEntryType.Information, 5000);
        }

        public void Log2EventLog(string EvtMsg, EventLogEntryType EvtType, int EventID)
        {
            string EvtSource = Application.ProductName + " " + Application.ProductVersion.ToString() + " AutoUpdate";
            // LOG to EventLog
            try
            {
                EventLog aLog = new EventLog();
                aLog.Source = EvtSource;
                aLog.WriteEntry(EvtMsg, EvtType, EventID);
            }
            catch { }
        }
        
        private bool CheckForUpdates()
        {
            // Check for updates at AU site
            // This method will read "au.xml" from AU site.
            FileVersionInfo AppVer = FileVersionInfo.GetVersionInfo(Application.ExecutablePath);
            int MyVersion = (AppVer.FileMajorPart * 100) + (AppVer.FileMinorPart * 10) + AppVer.FileBuildPart;
            if (ForceAU == true) MyVersion = 0;
            au_log += Application.ProductName.ToString() + "; " + Application.CompanyName.ToString();
            au_log += "\r\n" + Application.ExecutablePath;
            au_log += "; Version: " +Application.ProductVersion + " (" + MyVersion.ToString() + ")";
            au_log += "\r\n" + System.DateTime.Now.ToString();
            au_log += "\r\nAU site: " + au_url + "\r\n";
            try
            {
                XmlTextReader au_info_reader = new XmlTextReader(au_url + "au.xml");
                XmlDocument au_info = new XmlDocument();
                au_info.Load(au_info_reader);
                XmlNode au_info_element = au_info.DocumentElement;

                XmlNodeList au_files = au_info_element.SelectNodes("/AutoUpdate/file/name/text()");
                XmlNodeList au_files_check = au_info_element.SelectNodes("/AutoUpdate/file/check/text()");
                XmlNodeList au_versions = au_info_element.SelectNodes("/AutoUpdate/file/version/text()");
                for (int loop = 0; loop < au_files.Count; loop++)
                {
                    if (MyVersion < Convert.ToInt32(au_versions[loop].Value))
                    {
                        // Add file to download list...
                        auFilesToUpdate[auFilesToUpdateCount] = au_files[loop].Value;
                        try { auFilesToUpdate_check[auFilesToUpdateCount] = au_files_check[loop].Value; }
                        catch { auFilesToUpdate_check[auFilesToUpdateCount] = null; }
                        auFilesToUpdateCount++;
                    }
                }
            }
            catch
            {
                au_log += "\r\nNepoda�ilo se kontaktovat AutoUpdate.";
                return (false);
            }
            if (auFilesToUpdateCount > 0) { return (true); } else { return (false); }
        }

        private int DownloadFile(string remoteFileName, string localFileName)
        {
            int bytesdone = 0;
            Stream RStream = null;
            Stream LStream = null;
            WebResponse response = null;
            try
            {
                WebRequest request = WebRequest.Create(remoteFileName);
                if (request != null)
                {
                    response = request.GetResponse();
                    if (response != null)
                    {
                        au_log += "\r\nDOWNLOAD: " + remoteFileName + " (size: " + Convert.ToInt32(response.ContentLength) + " B";
                        NotifyText("Stahuji soubor\r\n" + remoteFileName + " (" + (Convert.ToInt32(response.ContentLength) / 1024) + " kB)");
                        RStream = response.GetResponseStream();
                        LStream = File.Create(localFileName);
                        byte[] buffer = new byte[1024];
                        int bytesRead;
                        do
                        {
                            Application.DoEvents();
                            bytesRead = RStream.Read(buffer, 0, buffer.Length);
                            LStream.Write(buffer, 0, bytesRead);
                            bytesdone += bytesRead;
                        } while (bytesRead > 0);
                        LStream.Flush();
                        au_log += " - downloaded " + bytesdone + " B)";
                        
                        if (bytesdone != Convert.ToInt32(response.ContentLength))
                        {
                            bytesdone = -1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                au_log += "\r\n" + ex.ToString();
                bytesdone = -1;
            }
            finally
            {
                if (response != null) response.Close();
                if (RStream != null) RStream.Close();
                if (LStream != null) LStream.Close();
            }
            return bytesdone;
        }

        private bool CheckFile(string localFileName, string checkValue)
        {
            string loc_checkValue = null;
            
            FileStream file = new FileStream(localFileName, FileMode.Open);
            CrcStream stream = new CrcStream(file);
            //Use the file somehow -- in this case, read it as a string
            StreamReader reader = new StreamReader(stream);
            string text = reader.ReadToEnd();
            loc_checkValue = stream.ReadCrc.ToString("X8");
            reader.Close();
            stream.Close();
            file.Close();
            au_log += "\r\nCHECK: " + localFileName.Replace(Application.StartupPath, "");
            au_log += "; CRC: " + loc_checkValue + ", should be: " + checkValue;
            if (loc_checkValue == checkValue)
            {
                au_log += " -OK-";
                return (true);
            }
            else
            {
                au_log += " -ERROR-";
                DeleteFiles_mask(localFileName.Replace(Application.StartupPath + "\\", ""));
                return (false);
            }
        }

        private void DeleteFiles_mask(string str_FileMask)
        {
            try
            {
                System.IO.DirectoryInfo dir = new System.IO.DirectoryInfo(Application.StartupPath);
                foreach (System.IO.FileInfo f in dir.GetFiles(str_FileMask))
                {
                    f.Delete();
                    au_log += "\r\nDELETE: " + f.Name.ToString();
                }
            }
            catch { /* IGNORE error... */ }
        }

        private void NotifyStart()
        {
            ico.Text = au_ico_Text;
            ico.BalloonTipTitle = au_ico_Text;

            ico.Icon = au_ico;
            ico.Visible = true;

            ico.BalloonTipText = "Prob�h� aktualizace aplikace...";
            ico.BalloonTipIcon = ToolTipIcon.Info;
            ico.ShowBalloonTip(3);
            Application.DoEvents();
        }

        private void NotifyEnd()
        {
            System.Threading.Thread.Sleep(5000);
            ico.Visible = false;
            ico.Dispose();
        }

        private void NotifyOK()
        {
            ico.BalloonTipText = "Aktualizace byla �sp�n� dokon�ena.\r\nPro dokon�en� aktualizace je pot�eba restartovat aplikaci.";
            ico.BalloonTipIcon = ToolTipIcon.Info;
            ico.ShowBalloonTip(5);
            Application.DoEvents();
            NotifyEnd();
            Application.Restart();
        }

        private void NotifyError()
        {
            ico.BalloonTipText = "B�hem aktualizace do�lo k chyb�. V�ce informac� najdete v aktualiza�n�m logu.";
            ico.BalloonTipIcon = ToolTipIcon.Error;
            ico.ShowBalloonTip(10);
            Application.DoEvents();
            NotifyEnd();
        }

        private void NotifyText(string Text)
        {
            ico.BalloonTipText = Text;
            ico.BalloonTipIcon = ToolTipIcon.Info;
            ico.ShowBalloonTip(5);
            Application.DoEvents();
        }

        public void DoUpdate()
        {
            NotifyStart();
            CheckForUpdates();
            au_log += "\r\nFiles to update: " + auFilesToUpdateCount.ToString();
            // Auto Update CleanUp
            DeleteFiles_mask("*.au");
            if (auFilesToUpdateCount > 0)
            {
                // Download All Files
                string localFile = null;
                string localTempFile = null;
                string localDownloadedFile = null;
                string remoteFile = null;
                bool CheckOK = false;

                //Download NEW files
                for (int loop = 0; loop < auFilesToUpdateCount; loop++)
                {
                    localFile = Application.StartupPath + "\\" + auFilesToUpdate[loop];
                    localTempFile = localFile + ".au";
                    localDownloadedFile = localFile + ".aud";
                    remoteFile = au_url + auFilesToUpdate[loop];

                    CheckOK = false;
                    if (File.Exists(localDownloadedFile))
                    {
                        // FILE ALREADY DOWNLOADED...
                        au_log += "\r\nFILE ALREADY DOWNLOADED: " + localDownloadedFile.Replace(Application.StartupPath, "");

                        CheckOK = CheckFile(localDownloadedFile, auFilesToUpdate_check[loop]);
                        if (CheckOK == false)
                        {
                            DeleteFiles_mask(localDownloadedFile.Replace(Application.StartupPath + "\\", ""));
                        }
                    }

                    if (CheckOK == false)
                    {
                        DownloadFile(remoteFile, localDownloadedFile);
                        CheckOK = CheckFile(localDownloadedFile, auFilesToUpdate_check[loop]);
                    }
                    if (CheckOK == false)
                    {
                        // ERROR DOWNLOADING FILE!
                        au_error = true;
                    }
                }

                // DO UPDATE
                if (au_error == false)
                {
                    for (int loop = 0; loop < auFilesToUpdateCount; loop++)
                    {
                        localFile = Application.StartupPath + "\\" + auFilesToUpdate[loop];
                        localTempFile = localFile + ".au";
                        localDownloadedFile = localFile + ".aud";

                        // Rename files to "*.au"
                        try
                        {
                            FileInfo fiM = new FileInfo(localFile);
                            fiM.MoveTo(localTempFile);
                            au_log += "\r\nSWITCH FILES: " + localFile.Replace(Application.StartupPath, "") + " -> " + localTempFile.Replace(Application.StartupPath, "");
                        }
                        catch { /* Soubor lokalne neexistuje! */ }

                        // Rename downloaded files to normal files...
                        try
                        {
                            FileInfo fiM = new FileInfo(localDownloadedFile);
                            fiM.MoveTo(localFile);
                            au_log += "\r\nSWITCH FILES: " + localDownloadedFile.Replace(Application.StartupPath, "") + " -> " + localFile.Replace(Application.StartupPath, "");
                        }
                        catch (Exception ex)
                        {
                            au_log += "\r\n" + ex.ToString();
                            /* TODO: ERROR! */
                        }
                    }
                    au_log += "\r\n\r\n=====< DONE >=====";
                    au_log += "\r\n" + System.DateTime.Now.ToString();
                    NotifyOK();
                } else { NotifyError(); }
            } else { NotifyEnd(); }
            SaveAuLog();
        }
    }
}
