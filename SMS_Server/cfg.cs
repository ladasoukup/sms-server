/*
 * Created by SharpDevelop.
 * User: Soukup
 * Date: 22.8.2005
 * Time: 18:03
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */

using System;
using System.Drawing;
using System.Windows.Forms;

namespace SMS_Server
{
	/// <summary>
	/// Description of cfg.
	/// </summary>
	public class cfg : System.Windows.Forms.Form
    {
		private System.Windows.Forms.TextBox cfg_SQL_db_table;
        private System.Windows.Forms.TextBox cfg_SQL_db;
		private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox cfg_SQL_host;
        private System.Windows.Forms.TextBox cfg_SQL_user;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button btn_save;
		private System.Windows.Forms.TextBox cfg_SQL_pass;
        private GroupBox groupBox4;
        private TextBox cfg_smtp_to;
        private Label label12;
        private TextBox cfg_smtp_from;
        private Label label11;
        private TextBox cfg_smtp_server;
        private Label label10;
        private Label label1;
        private MaskedTextBox cfg_smsGate;
        private Label label2;
        private TextBox cfg_smsSendID;
        private ComboBox RS232_COM_speed;
        private Label label3;
        private CheckBox RS232_AutoConnect;
        private ComboBox RS232_useCom;
        private Label label9;
        private Label label13;
        private TextBox cfg_AppID;
		private MainForm opener;
		public cfg(MainForm sender)
		{
			InitializeComponent();
			opener = sender;
		}
		
		#region Windows Forms Designer generated code
		/// <summary>
		/// This method is required for Windows Forms designer support.
		/// Do not change the method contents inside the source code editor. The Forms designer might
		/// not be able to load this method if it was changed manually.
		/// </summary>
		private void InitializeComponent() {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(cfg));
            this.cfg_SQL_pass = new System.Windows.Forms.TextBox();
            this.btn_save = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cfg_SQL_user = new System.Windows.Forms.TextBox();
            this.cfg_SQL_host = new System.Windows.Forms.TextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cfg_SQL_db_table = new System.Windows.Forms.TextBox();
            this.cfg_SQL_db = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cfg_smsSendID = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cfg_smsGate = new System.Windows.Forms.MaskedTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.cfg_smtp_to = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cfg_smtp_from = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.cfg_smtp_server = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.RS232_COM_speed = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.RS232_AutoConnect = new System.Windows.Forms.CheckBox();
            this.RS232_useCom = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.cfg_AppID = new System.Windows.Forms.TextBox();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // cfg_SQL_pass
            // 
            this.cfg_SQL_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cfg_SQL_pass.Location = new System.Drawing.Point(104, 72);
            this.cfg_SQL_pass.Name = "cfg_SQL_pass";
            this.cfg_SQL_pass.PasswordChar = '#';
            this.cfg_SQL_pass.Size = new System.Drawing.Size(160, 21);
            this.cfg_SQL_pass.TabIndex = 13;
            // 
            // btn_save
            // 
            this.btn_save.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_save.Location = new System.Drawing.Point(8, 217);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(272, 23);
            this.btn_save.TabIndex = 3;
            this.btn_save.Text = "Uložit nastavení";
            this.btn_save.Click += new System.EventHandler(this.btn_save_click);
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(8, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 24);
            this.label8.TabIndex = 16;
            this.label8.Text = "Tabulka:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(8, 72);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 24);
            this.label4.TabIndex = 10;
            this.label4.Text = "Heslo:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 24);
            this.label5.TabIndex = 9;
            this.label5.Text = "Uživatel:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(8, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(88, 24);
            this.label6.TabIndex = 8;
            this.label6.Text = "Server:";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(8, 96);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(88, 24);
            this.label7.TabIndex = 14;
            this.label7.Text = "Databáze:";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_SQL_user
            // 
            this.cfg_SQL_user.Location = new System.Drawing.Point(104, 48);
            this.cfg_SQL_user.Name = "cfg_SQL_user";
            this.cfg_SQL_user.Size = new System.Drawing.Size(160, 20);
            this.cfg_SQL_user.TabIndex = 12;
            // 
            // cfg_SQL_host
            // 
            this.cfg_SQL_host.Location = new System.Drawing.Point(104, 24);
            this.cfg_SQL_host.Name = "cfg_SQL_host";
            this.cfg_SQL_host.Size = new System.Drawing.Size(160, 20);
            this.cfg_SQL_host.TabIndex = 11;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.cfg_SQL_db_table);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cfg_SQL_db);
            this.groupBox2.Controls.Add(this.cfg_SQL_host);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.cfg_SQL_user);
            this.groupBox2.Controls.Add(this.cfg_SQL_pass);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox2.Location = new System.Drawing.Point(8, 59);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(272, 152);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "MySQL";
            // 
            // cfg_SQL_db_table
            // 
            this.cfg_SQL_db_table.Location = new System.Drawing.Point(104, 120);
            this.cfg_SQL_db_table.Name = "cfg_SQL_db_table";
            this.cfg_SQL_db_table.Size = new System.Drawing.Size(160, 20);
            this.cfg_SQL_db_table.TabIndex = 17;
            // 
            // cfg_SQL_db
            // 
            this.cfg_SQL_db.Location = new System.Drawing.Point(104, 96);
            this.cfg_SQL_db.Name = "cfg_SQL_db";
            this.cfg_SQL_db.Size = new System.Drawing.Size(160, 20);
            this.cfg_SQL_db.TabIndex = 15;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cfg_smsSendID);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.cfg_smsGate);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox3.Location = new System.Drawing.Point(286, 161);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(272, 79);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Obecné";
            // 
            // cfg_smsSendID
            // 
            this.cfg_smsSendID.Location = new System.Drawing.Point(189, 44);
            this.cfg_smsSendID.Name = "cfg_smsSendID";
            this.cfg_smsSendID.Size = new System.Drawing.Size(73, 20);
            this.cfg_smsSendID.TabIndex = 16;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(8, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 24);
            this.label2.TabIndex = 15;
            this.label2.Text = "Složka odeslané (ID):";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_smsGate
            // 
            this.cfg_smsGate.Location = new System.Drawing.Point(111, 18);
            this.cfg_smsGate.Mask = "000000000";
            this.cfg_smsGate.Name = "cfg_smsGate";
            this.cfg_smsGate.Size = new System.Drawing.Size(151, 20);
            this.cfg_smsGate.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(8, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 24);
            this.label1.TabIndex = 12;
            this.label1.Text = "SMS_gate:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.cfg_smtp_to);
            this.groupBox4.Controls.Add(this.label12);
            this.groupBox4.Controls.Add(this.cfg_smtp_from);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Controls.Add(this.cfg_smtp_server);
            this.groupBox4.Controls.Add(this.label10);
            this.groupBox4.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.groupBox4.Location = new System.Drawing.Point(286, 59);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(272, 96);
            this.groupBox4.TabIndex = 4;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "SMTP";
            // 
            // cfg_smtp_to
            // 
            this.cfg_smtp_to.Location = new System.Drawing.Point(102, 68);
            this.cfg_smtp_to.Name = "cfg_smtp_to";
            this.cfg_smtp_to.Size = new System.Drawing.Size(160, 20);
            this.cfg_smtp_to.TabIndex = 10;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(8, 68);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 24);
            this.label12.TabIndex = 9;
            this.label12.Text = "Příjemce:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_smtp_from
            // 
            this.cfg_smtp_from.Location = new System.Drawing.Point(102, 42);
            this.cfg_smtp_from.Name = "cfg_smtp_from";
            this.cfg_smtp_from.Size = new System.Drawing.Size(160, 20);
            this.cfg_smtp_from.TabIndex = 8;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(6, 42);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 24);
            this.label11.TabIndex = 7;
            this.label11.Text = "Odesílatel:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // cfg_smtp_server
            // 
            this.cfg_smtp_server.Location = new System.Drawing.Point(102, 16);
            this.cfg_smtp_server.Name = "cfg_smtp_server";
            this.cfg_smtp_server.Size = new System.Drawing.Size(160, 20);
            this.cfg_smtp_server.TabIndex = 6;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Tahoma", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(6, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 24);
            this.label10.TabIndex = 5;
            this.label10.Text = "Server:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // RS232_COM_speed
            // 
            this.RS232_COM_speed.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.RS232_COM_speed.FormattingEnabled = true;
            this.RS232_COM_speed.Items.AddRange(new object[] {
            "110",
            "300",
            "1200",
            "2400",
            "4800",
            "9600",
            "19200",
            "38400",
            "57600",
            "115200",
            "230400",
            "460800",
            "921600"});
            this.RS232_COM_speed.Location = new System.Drawing.Point(169, 32);
            this.RS232_COM_speed.Name = "RS232_COM_speed";
            this.RS232_COM_speed.Size = new System.Drawing.Size(92, 21);
            this.RS232_COM_speed.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(111, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Rychlost:";
            // 
            // RS232_AutoConnect
            // 
            this.RS232_AutoConnect.AutoSize = true;
            this.RS232_AutoConnect.Location = new System.Drawing.Point(267, 34);
            this.RS232_AutoConnect.Name = "RS232_AutoConnect";
            this.RS232_AutoConnect.Size = new System.Drawing.Size(96, 17);
            this.RS232_AutoConnect.TabIndex = 14;
            this.RS232_AutoConnect.Text = "Auto. připojení";
            this.RS232_AutoConnect.UseVisualStyleBackColor = true;
            // 
            // RS232_useCom
            // 
            this.RS232_useCom.FormattingEnabled = true;
            this.RS232_useCom.Location = new System.Drawing.Point(45, 32);
            this.RS232_useCom.Name = "RS232_useCom";
            this.RS232_useCom.Size = new System.Drawing.Size(60, 21);
            this.RS232_useCom.TabIndex = 13;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(5, 35);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 13);
            this.label9.TabIndex = 12;
            this.label9.Text = "COM:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(5, 9);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 13);
            this.label13.TabIndex = 17;
            this.label13.Text = "ID aplikace:";
            // 
            // cfg_AppID
            // 
            this.cfg_AppID.Location = new System.Drawing.Point(75, 6);
            this.cfg_AppID.MaxLength = 32;
            this.cfg_AppID.Name = "cfg_AppID";
            this.cfg_AppID.Size = new System.Drawing.Size(186, 20);
            this.cfg_AppID.TabIndex = 18;
            // 
            // cfg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(566, 248);
            this.Controls.Add(this.cfg_AppID);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.RS232_COM_speed);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.RS232_AutoConnect);
            this.Controls.Add(this.RS232_useCom);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.btn_save);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "cfg";
            this.Text = "ČTK server - nastavení";
            this.Load += new System.EventHandler(this.CfgLoad);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

		}
		#endregion
		void CfgLoad(object sender, System.EventArgs e)
		{
            string S = "";
            int I = 16;

            S = opener.ini.IniReadValue("global", "AppID");
            if (S != "") cfg_AppID.Text = S;

            S = opener.ini.IniReadValue("COM", "MaxCOM");
            if (S != "") try { I = Convert.ToInt32(S); }
                catch { opener.LogDebug("MaxCOM - invalid value", "INI"); }
            for (int loop = 1; loop <= I; loop++) { RS232_useCom.Items.Add("COM" + loop.ToString()); }
            RS232_useCom.SelectedIndex = 0;

            S = opener.ini.IniReadValue("COM", "Port");
            if (S != "") RS232_useCom.Text = S;

            RS232_COM_speed.Text = "9600";
            S = opener.ini.IniReadValue("COM", "PortSpeed");
            if (S != "") RS232_COM_speed.Text = S;

            S = opener.ini.IniReadValue("COM", "AutoConnect");
            if (S == "True") RS232_AutoConnect.Checked = true;

            cfg_SQL_host.Text = opener.ini.IniReadValue("SQL", "sql_host");
			cfg_SQL_user.Text = opener.ini.IniReadValue("SQL", "sql_user");
			cfg_SQL_db.Text = opener.ini.IniReadValue("SQL", "sql_db");
			cfg_SQL_db_table.Text = opener.ini.IniReadValue("SQL", "sql_db_table");

            cfg_smtp_server.Text = opener.ini.IniReadValue("smtp", "MailServer");
            cfg_smtp_to.Text = opener.ini.IniReadValue("smtp", "MailTo");
            cfg_smtp_from.Text = opener.ini.IniReadValue("smtp", "MailFrom");

            cfg_smsGate.Text = opener.ini.IniReadValue("SMS", "sms_gate");
            cfg_smsSendID.Text = opener.ini.IniReadValue("SMS", "sms_sendID");
		}
		
		void btn_save_click(object sender, System.EventArgs e)
		{
            opener.ini.IniWriteValue("global", "AppID", cfg_AppID.Text);

            opener.ini.IniWriteValue("SQL", "sql_host", cfg_SQL_host.Text);
			opener.ini.IniWriteValue("SQL", "sql_user", cfg_SQL_user.Text);
            if (cfg_SQL_pass.Text != "")
            {
                opener.ini.IniWriteEncValue("SQL", "sql_pass", cfg_SQL_pass.Text);
            }
			opener.ini.IniWriteValue("SQL", "sql_db", cfg_SQL_db.Text);
			opener.ini.IniWriteValue("SQL", "sql_db_table", cfg_SQL_db_table.Text);
			
            opener.ini.IniWriteValue("smtp", "MailServer", cfg_smtp_server.Text);
            opener.ini.IniWriteValue("smtp", "MailTo", cfg_smtp_to.Text);
            opener.ini.IniWriteValue("smtp", "MailFrom", cfg_smtp_from.Text);

            opener.ini.IniWriteValue("SMS", "sms_gate", cfg_smsGate.Text);
            opener.ini.IniWriteValue("SMS", "sms_sendID", cfg_smsSendID.Text);

            opener.ini.IniWriteValue("COM", "Port", RS232_useCom.Text.ToString());
            opener.ini.IniWriteValue("COM", "PortSpeed", RS232_COM_speed.Text.ToString());
            opener.ini.IniWriteValue("COM", "AutoConnect", RS232_AutoConnect.Checked.ToString());

            opener.UseCOM = RS232_useCom.Text;
            opener.UseCOMspeed = RS232_COM_speed.Text;
            opener.UseCOMautoConnect = RS232_AutoConnect.Checked;

            this.Close();
		}
	}
}
